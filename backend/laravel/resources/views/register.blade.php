<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>User Registration Form</title>
    <script src="https://cdn.tailwindcss.com"></script>
</head>
<body class="bg-gray-200">
<div class="container mx-auto p-8">
    <div class="flex items-center justify-center h-screen">
        <div class="w-full max-w-sm">
            <form method="post" action="{{ route('users.store') }}" enctype="multipart/form-data">
                <div class="mb-4">
                    <label class="block text-gray-700 font-bold mb-2" for="name">
                        Name
                    </label>
                    <input
                        class="bg-white border border-gray-400 rounded p-2 w-full @error('name') border-red-500 @enderror"
                        type="text"
                        name="name"
                        id="name"
                        value="{{ old('name') }}"
                        required
                    >
                    @error('name')
                    <p class="text-red-500 text-xs italic mt-4">{{ $message }}</p>
                    @enderror
                </div>
                <div class="mb-4">
                    <label class="block text-gray-700 font-bold mb-2" for="email">
                        Email
                    </label>
                    <input
                        class="bg-white border border-gray-400 rounded p-2 w-full @error('email') border-red-500 @enderror"
                        type="email"
                        name="email"
                        id="email"
                        value="{{ old('email') }}"
                        required
                    >
                    @error('email')
                    <p class="text-red-500 text-xs italic mt-4">{{ $message }}</p>
                    @enderror
                </div>
                <div class="mb-4">
                    <label class="block text-gray-700 font-bold mb-2" for="phone">
                        Phone
                    </label>
                    <input
                        class="bg-white border border-gray-400 rounded p-2 w-full @error('phone') border-red-500 @enderror"
                        type="text"
                        name="phone"
                        id="phone"
                        value="{{ old('phone') }}"
                        required
                    >
                    @error('phone')
                    <p class="text-red-500 text-xs italic mt-4">{{ $message }}</p>
                    @enderror
                </div>
                <div class="mb-4">
                    <label class="block text-gray-700 font-bold mb-2" for="position_id">
                        Position
                    </label>
                    <select
                        class="bg-white border border-gray-400 rounded p-2 w-full @error('position_id') border-red-500 @enderror"
                        name="position_id"
                        id="position_id"
                        required
                    >
                        <option value="">Select Position</option>
                        @foreach($positions as $position)
                            <option value="{{ $position->id }}">{{ $position->name }}</option>
                        @endforeach
                    </select>
                    @error('position_id')
                    <p class="text-red-500 text-xs italic mt-4">{{ $message }}</p>
                    @enderror
                </div>
                <div class="mb-4">
                    <label class="block text-gray-700 font-bold mb-2" for="photo">
                        Photo
                    </label>
                    <input
                        class="bg-white border border-gray-400 rounded p-2 w-full @error('photo') border-red-500 @enderror"
                        type="file"
                        name="photo"
                        id="photo"
                        value="{{ old('photo') }}"
                        required
                    >
                    @error('photo')
                    <p class="text-red-500 text-xs italic mt-4">{{ $message }}</p>
                    @enderror
                </div>
                <div class="mb-4">
                    <button
                        class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded"
                        type="submit"
                    >
                        Register
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>
</body>
</html>
