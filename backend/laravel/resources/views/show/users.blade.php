<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>User Registration Form</title>
    <script src="https://cdn.tailwindcss.com"></script>
    <script src="https://cdn.jsdelivr.net/gh/alpinejs/alpine@2.x.x/dist/alpine.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>
</head>
<body class="bg-yellow-300">
<table x-ref="tableBody" class="table-auto w-full">
    <thead>
    <tr class="bg-blue-500 text-white">
        <th class="px-4 py-2">ID</th>
        <th class="px-4 py-2">Name</th>
        <th class="px-4 py-2">Email</th>
    </tr>
    </thead>
    <tbody>
    @foreach ($users as $user)
        <tr>
            <td class="border px-4 py-2">{{ $user->id }}</td>
            <td class="border px-4 py-2">{{ $user->name }}</td>
            <td class="border px-4 py-2">{{ $user->email }}</td>
        </tr>
    @endforeach
    </tbody>
</table>

<div class="flex justify-center mt-8">
    <button x-data="{ offset: 6, count: 6 , showMore() {
        axios.get('/api/users?offset=' + this.offset + '&count=' + this.count)
            .then(response => {
                let tableBody = document.querySelector('table tbody');
                response.data.users.forEach(user => {
                    this.$nextTick(() => {
                        let newRow = document.createElement('tr');
                        newRow.innerHTML = `
                            <td class='border px-4 py-2'>${user.id}</td>
                            <td class='border px-4 py-2'>${user.name}</td>
                            <td class='border px-4 py-2'>${user.email}</td>`;
                            console.log(this.$refs);
                            tableBody.appendChild(newRow);
                    });
                });
                this.offset += this.count;
            }).catch(error => {
                console.error(error);
            });
        }
    }" @click="showMore()" class="bg-blue-500 hover:bg-blue-400 text-white font-bold py-2 px-4 mr-5 rounded">
        Show More
    </button>
    <a href="/register" class="bg-blue-500 hover:bg-blue-400 text-white font-bold py-2 px-4 rounded">
        Register
    </a>
</div>
</body>
</html>
