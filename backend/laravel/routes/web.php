<?php

use App\Models\Position;
use App\Models\User;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    $users = User::query()->paginate(6);
    return view('show.users', compact('users'));
});

Route::get('/register', function () {
    return view('register')->with('positions', Position::all());
})->name('register');
