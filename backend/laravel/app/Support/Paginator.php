<?php

namespace App\Support;

class Paginator
{
    /**
     * @param int $current
     * @param int $total
     * @param int $count
     * @param string $type
     * @param int $increment
     * @return array<string, string|null>
     */
    public static function getUrls(
        int    $current,
        int    $total,
        int    $count,
        string $type = 'offset',
        int    $increment = 1
    ): array
    {
        $next = $current + $increment;
        $prev = $current - $increment;

        $nextUrl = $next < $total
            ? route('users.index', [$type => $next, 'count' => $count])
            : null;
        $prevUrl = $prev > 0
            ? route('users.index', [$type => $prev, 'count' => $count])
            : null;

        return [
            'next_url' => $nextUrl,
            'prev_url' => $prevUrl,
        ];
    }

    /**
     * @param int $offset
     * @param int $count
     * @param int $totalUsers
     * @return array<string, string|null>
     */
    public static function getUrlsForOffset(int $offset, int $count, int $totalUsers): array
    {
        return self::getUrls($offset, $totalUsers, $count, 'offset', $count);
    }

    /**
     * @param int $page
     * @param int $count
     * @param int $totalPages
     * @return array<string, string|null>
     */
    public static function getUrlsForPage(int $page, int $count, int $totalPages): array
    {
        return self::getUrls($page, $totalPages, $count, 'page');
    }
}
