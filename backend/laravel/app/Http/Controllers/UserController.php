<?php

namespace App\Http\Controllers;

use App\Http\Requests\IndexUsersRequest;
use App\Http\Requests\ShowUserRequest;
use App\Http\Requests\StoreUserRequest;
use App\Models\User;
use App\Services\TinyPngService;
use App\Support\Paginator;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Str;

class UserController extends Controller
{
    /**
     * @param IndexUsersRequest $request
     * @return JsonResponse
     */
    public function index(IndexUsersRequest $request): JsonResponse
    {
        try {
            $count = $request->validated('count', 10);
            $offset = $request->validated('offset');
            $page = $request->validated('page', 1);

            $total_users = User::query()->count();
            $total_pages = ceil($total_users / $count);

            if (!$total_users) {
                return response()->json([
                    'success' => false,
                    'message' => 'No users found'
                ], 404);
            }

            if ($offset && ($offset >= $total_users || $offset < 0)) {
                return response()->json([
                    'message' => 'Page not found'
                ], 404);
            }

            if ($page && ($page > $total_pages || $page < 1)) {
                return response()->json([
                    'error' => 'Page not found'
                ], 404);
            }

            $links = $offset
                ? Paginator::getUrlsForOffset($offset, $count, $total_users)
                : Paginator::getUrlsForPage($page, $count, $total_pages);

            return response()->json([
                'success' => true,
                'total_pages' => $total_pages,
                'total_users' => $total_users,
                'count' => $count,
                $offset ? 'offset' : 'page' => $offset ?: $page,
                'links' => $links,
                'users' => User::getUsersCollection($count, $offset)
            ]);
        } catch (Exception $e) {
            return response()->json([
                'success' => false,
                'message' => $e->getMessage()
            ], 500);
        }
    }

    /**
     * @param ShowUserRequest $request
     * @return JsonResponse
     */
    public function show(ShowUserRequest $request): JsonResponse
    {
        try {
            if (!$user = User::getUser($request->validated('id'))) {
                return response()->json([
                    'success' => false,
                    'message' => 'The user with the requested identifier does not exist',
                    'fails' => [
                        'user_id' => 'User not found'
                    ]
                ], 404);
            }

            return response()->json([
                'success' => true,
                'user' => $user
            ]);
        } catch (Exception $e) {
            return response()->json([
                'success' => false,
                'message' => $e->getMessage()
            ], 500);
        }
    }

    /**
     * @param StoreUserRequest $request
     * @return JsonResponse
     */
    public function store(StoreUserRequest $request): JsonResponse
    {
        try {
            $validated = $request->validated();
            $photo = $request->file('photo');
            $validated['photo'] = TinyPngService::optimizeAndSaveImage($photo);
            $validated['registration_timestamp'] = time();
            $user = User::query()->create($validated);

            return response()->json([
                'success' => true,
                'user_id' => $user->id,
                'message' => 'New user successfully registered'
            ]);
        } catch (Exception $e) {
            return response()->json([
                'success' => false,
                'message' => $e->getMessage()
            ], 500);
        }
    }

    /**
     * @return JsonResponse
     */
    public function getToken(): JsonResponse
    {
        $token = encrypt(Str::random(32));
        Cache::put('Token', $token, 40);

        return response()->json([
            'success' => true,
            'token' => $token,
        ]);
    }
}
