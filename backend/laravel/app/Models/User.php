<?php

namespace App\Models;

// use Illuminate\Contracts\Auth\MustVerifyEmail;
use App\Http\Resources\UserResource;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'email',
        'phone',
        'photo',
        'password',
        'position_id',
        'registration_timestamp',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * @return BelongsTo
     */
    public function position(): BelongsTo
    {
        return $this->belongsTo(Position::class);
    }

    /**
     * @param int $count
     * @param int|null $offset
     * @return AnonymousResourceCollection
     */
    public static function getUsersCollection(int $count, ?int $offset): AnonymousResourceCollection
    {
        $users = User::query()
            ->when($offset, fn($query) => $query->offset($offset))
            ->limit($count)
            ->get();
        return UserResource::collection($users);
    }

    /**
     * @param int|null $id
     * @return UserResource|null
     */
    public static function getUser(?int $id): ?UserResource
    {
        $user = User::query()->find($id);
        return $user ? new UserResource($user) : null;
    }
}
