<?php

namespace App\Services;

use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Tinify\Tinify;

/**
 * Class TinyPngService
 *
 * This class provides the logic for the TinyPngService service.
 */
class TinyPngService
{
    /**
     * Properties for the service.
     *
     * @var array
     */
    protected array $properties = [];

    /**
     * TinyPngService constructor.
     *
     * @param array $properties
     */
    public function __construct(array $properties = [])
    {
        $this->properties = $properties;
    }

    /**
     * @param UploadedFile $image
     * @return false|string
     */
    public static function optimizeAndSaveImage(UploadedFile $image): false|string
    {
        try {
            Tinify::setKey(env('TINY_PNG_API_KEY'));
            $compressedImage = \Tinify\fromFile($image->path())->resize([
                'method' => 'fit',
                'width' => 70,
                'height' => 70
            ])->toBuffer();
            $imagePath = 'images/' . $image->hashName();
            Storage::disk('public')->put($imagePath, $compressedImage);
            return Storage::disk('public')->url($imagePath);
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }
}
