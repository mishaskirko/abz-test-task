<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\File;

class CreateService extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'make:service {name}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a new service';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int|void
     */
    public function handle()
    {
        $name = $this->argument('name');
        $servicePath = app_path("Services/{$name}.php");

        if (File::exists($servicePath)) {
            $this->error("Service {$name} already exists!");
            return 1;
        }

        if (!File::exists(app_path('Services'))) {
            File::makeDirectory(app_path('Services'));
        }

        File::put($servicePath, $this->serviceStub($name));

        $this->info("Service created successfully at app/Services/{$name}.php");
    }

    /**
     * Get the stub file for the generator.
     *
     * @param $name
     * @return string
     */
    protected function serviceStub($name): string
    {
        return <<<EOF
        <?php

        namespace App\Services;

        /**
         * Class {$name}
         *
         * This class provides the logic for the {$name} service.
         */
        class {$name}
        {
            /**
             * Properties for the service.
             *
             * @var array
             */
            protected \$properties = [];

            /**
             * {$name} constructor.
             *
             * @param array \$properties
             */
            public function __construct(array \$properties = [])
            {
                \$this->properties = \$properties;
            }

            /**
             * Main logic for the service.
             *
             * @return mixed
             */
            public function handle()
            {
                // Service logic goes here
            }
        }
        EOF;
    }
}
