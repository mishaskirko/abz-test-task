include .env

up: # create and start containers
	@docker compose -f ${DOCKER_CONFIG} up -d

down: # stop and destroy containers
	@docker compose -f ${DOCKER_CONFIG} down

down-volume: #  WARNING: stop and destroy containers with volumes
	@docker compose -f ${DOCKER_CONFIG} down -v

start: # start already created containers
	@docker compose -f ${DOCKER_CONFIG} start

stop: # stop containers, but not destroy
	@docker compose -f ${DOCKER_CONFIG} stop

ps: # show started containers and their status
	@docker compose -f ${DOCKER_CONFIG} ps

build: # build all dockerfile, if not built yet
	@docker compose -f ${DOCKER_CONFIG} build

connect-app: # app shell
	@docker compose -f ${DOCKER_CONFIG} exec -u www -w /www/backend/laravel app sh

connect-app-root: # app shell
	@docker compose -f ${DOCKER_CONFIG} exec -w /www/backend/laravel app sh

connect-nginx: # nginx shell
	@docker compose -f ${DOCKER_CONFIG} exec -w /www nginx sh

nginx-reload: # nginx reload
	@docker compose -f ${DOCKER_CONFIG} exec -w /www nginx nginx -s reload

connect-db: # database shell
	@docker compose -f ${DOCKER_CONFIG} exec db bash

connect-tinker: # tinker shell
	@docker compose -f ${DOCKER_CONFIG} exec -u www -w /www/backend/laravel app php artisan ti

database-open:
	@docker compose -f ${DOCKER_CONFIG} exec db mysql -uroot -p${DOCKER_PASSWORD} ${DOCKER_DATABASE}

database-dump: # dump database
	@docker compose -f ${DOCKER_CONFIG} exec db bash -c "mysqldump ${DOCKER_DATABASE} -u${DOCKER_USERNAME} -p${DOCKER_PASSWORD} 2> /dev/null" > laravel/database/snapshots/dump_`date +%F_%R`.sql
	@echo dump_`date +%F_%R`.sql created successfully.

database-import:
	@laravel/database/snapshots/database_import.sh

laravel: # install laravel from scratch
	@docker compose -f ${DOCKER_CONFIG} exec -u www -w /www app composer create-project --prefer-dist laravel/laravel /www/backend/laravel "9.*"

vendor: # composer install
	@docker compose -f ${DOCKER_CONFIG} exec -u www -w /www/backend/laravel app composer install

vendor-publish: # vendor publish
	@docker compose -f ${DOCKER_CONFIG} exec -u www -w /www/backend/laravel app php artisan vendor:publish

key: # generate application key
	@docker compose -f ${DOCKER_CONFIG} exec -u www -w /www/backend/laravel app php artisan key:generate

fresh: # refresh the database and run all database seeds
	@docker compose -f ${DOCKER_CONFIG} exec -u www -w /www/backend/laravel app php artisan migrate:fresh --seed

passport: # refresh the passport jwt secret
	@docker compose -f ${DOCKER_CONFIG} exec -u www -w /www/backend/laravel app php artisan passport:client --personal

swagger: # generate swagger documentation
	@docker compose -f ${DOCKER_CONFIG} exec -u www -w /www/backend/laravel app  php artisan l5-swagger:generate

composer-dump: # composer dump-autoload
	@docker compose -f ${DOCKER_CONFIG} exec -u www -w /www/backend/laravel app composer dump-autoload

composer-install: # composer install
	@docker compose -f ${DOCKER_CONFIG} exec -u www -w /www/backend/laravel app composer install

composer-update: # composer update
	@docker compose -f ${DOCKER_CONFIG} exec -u www -w /www/backend/laravel app composer update

test: # run all tests
	@docker compose -f ${DOCKER_CONFIG} exec -u www -w /www/backend/laravel app php vendor/bin/phpunit

model: # create model name=[modelName]
	@docker compose -f ${DOCKER_CONFIG} exec -u www -w /www/backend/laravel app php artisan make:model $(name) -a

component: # create component name=[componentName]
	@docker compose -f ${DOCKER_CONFIG} exec -u www -w /www/backend/laravel app php artisan make:component $(name)

controller: # create controller name=[controllerName]
	@docker compose -f ${DOCKER_CONFIG} exec -u www -w /www/backend/laravel app php artisan make:controller $(name)

migration: # create migration name=[migrationName]
	@docker compose -f ${DOCKER_CONFIG} exec -u www -w /www/backend/laravel app php artisan make:migration $(name)

seeder: # create seeder name=[seederName]
	@docker compose -f ${DOCKER_CONFIG} exec -u www -w /www/backend/laravel app php artisan make:seeder $(name)

request: # create request name=[requestName]
	@docker compose -f ${DOCKER_CONFIG} exec -u www -w /www/backend/laravel app php artisan make:request $(name)

resource: # create resource name=[resourceName]
	@docker compose -f ${DOCKER_CONFIG} exec -u www -w /www/backend/laravel app php artisan make:resource $(name)

middleware: # create middleware name=[middlewareName]
	@docker compose -f ${DOCKER_CONFIG} exec -u www -w /www/backend/laravel app php artisan make:middleware $(name)

command: # create command name=[commandName]
	@docker compose -f ${DOCKER_CONFIG} exec -u www -w /www/backend/laravel app php artisan make:command $(name)

service: # create service name=[serviceName]
	@docker compose -f ${DOCKER_CONFIG} exec -u www -w /www/backend/laravel app php artisan make:service $(name)

optimize-cache: # optimize cache
	@docker compose -f ${DOCKER_CONFIG} exec -u www -w /www/backend/laravel app php artisan optimize:clear

clear-app-cache: # clear app cache
	@docker compose -f ${DOCKER_CONFIG} exec -u www -w /www/backend/laravel app php artisan cache:clear

clear-config-cache: # clear config cache
	@docker compose -f ${DOCKER_CONFIG} exec -u www -w /www/backend/laravel app php artisan config:clear

clear-route-cache: # clear route cache
	@docker compose -f ${DOCKER_CONFIG} exec -u www -w /www/backend/laravel app php artisan route:clear

clear-view-cache: # clear view cache
	@docker compose -f ${DOCKER_CONFIG} exec -u www -w /www/backend/laravel app php artisan view:clear

clear-compiled-cache: # clear compiled cache
	@docker compose -f ${DOCKER_CONFIG} exec -u www -w /www/backend/laravel app php artisan clear-compiled

route-list: # list all routes
	@docker compose -f ${DOCKER_CONFIG} exec -u www -w /www/backend/laravel app php artisan route:list

storage-link: # create storage link
	@docker compose -f ${DOCKER_CONFIG} exec -u www -w /www/backend/laravel app php artisan storage:link